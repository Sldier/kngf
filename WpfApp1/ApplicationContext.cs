﻿using WpfApp1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data;

namespace WpfApp1
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }        
        public DbSet<Company> Companies { get; set; }
        public DbSet<EmployeeCompany> EmployeeCompany { get; set; }        

        public List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map)
        {   
            using (var command = Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                this.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    var entities = new List<T>();

                    while (result.Read())
                    {
                        entities.Add(map(result));
                    }

                    return entities;
                }
            }            
        }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=base.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmployeeCompany>()
            .HasKey(t => new { t.EmployeeId, t.CompanyId });

            modelBuilder.Entity<EmployeeCompany>()
                .HasOne(sc => sc.Employee)
                .WithMany(s => s.EmployeeCompanies)
                .HasForeignKey(sc => sc.EmployeeId);

            
            modelBuilder.Entity<EmployeeCompany>()
                .HasOne(sc => sc.Company)
                .WithMany(c => c.EmployeeCompanies)
                .HasForeignKey(sc => sc.CompanyId);            
        }
    }
}
