﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.ViewModels;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            /// Паттерны: IoC.
            NinjectModule registrations = new NinjectRegistrations();
            IKernel kernel = new StandardKernel(registrations);
            kernel.Bind<EmployeeWindowViewModel>().ToSelf();
            kernel.Bind<MainWindowViewModel>().ToSelf();
            kernel.Bind<MainWindow>().ToSelf();

            var mainWindow = kernel.Get<MainWindow>();
            var data = kernel.Get<MainWindowViewModel>();
            mainWindow.DataContext = data;
            mainWindow.Show();

            base.OnStartup(e);
        }
    }
}
