﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class EmployeesWithMaximumSalary
    {
        public string CompanyName { get; set; }
        public string EmployeeName { get; set; }
        public string Salary { get; set; }
    }
}
