﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class EmployeeCompany
    {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public string Position { get; set; }
        public decimal Salary { get; set; }
    }
}
