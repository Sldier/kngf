﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class Employee : BaseModel
    {
        
        private int employeeId;
        private string name;
        private string address;
        private string telephone;
        private decimal salary;
        private bool mark;

        [Key]
        public int EmployeeId
        {
            get
            {
                return employeeId;
            }
            set
            {
                employeeId = value;
                OnPropertyChanged(nameof(EmployeeId));
            }
        }
        [Required]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                OnPropertyChanged(nameof(Address));
            }
        }
        public string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                telephone = value;
                OnPropertyChanged(nameof(Telephone));
            }
        }

        public List<EmployeeCompany> EmployeeCompanies { get; set; }

        [NotMapped]
        public decimal Salary
        {
            get
            {
                return salary;
            }
            set
            {
                salary = value;
                OnPropertyChanged(nameof(Salary));
            }
        }

        [NotMapped]
        public bool Mark {
            get
            {
                return mark;
            }
            set
            {
                mark = value;
                OnPropertyChanged(nameof(Mark));
            }
        }

        public Employee()
        {
            EmployeeCompanies = new List<EmployeeCompany>();
        }
    }
}
