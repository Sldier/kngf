﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class Company : BaseModel
    {
        
        private int companyId;
        private string name;
        private bool mark;

        [Key]
        public int CompanyId
        {
            get
            {
                return companyId;
            }

            set
            {
                companyId = value;
                OnPropertyChanged(nameof(CompanyId));
            }
        }

        [Required]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public List<EmployeeCompany> EmployeeCompanies { get; set; }

        [NotMapped]
        public bool Mark
        {
            get
            {
                return mark;
            }
            set
            {
                mark = value;
                OnPropertyChanged(nameof(Mark));
            }
        }
        
        public Company()
        {
            EmployeeCompanies = new List<EmployeeCompany>();
        }
    }
}
