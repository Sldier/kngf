﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.Models
{
    public class MaxSalaryCompanies
    {
        public int Id { get; set; }
        public decimal Salary { get; set; }
    }
}
