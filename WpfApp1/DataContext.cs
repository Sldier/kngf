﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Models;

namespace WpfApp1
{
    public class DataContext : iDataContext
    {
        public List<Employee> GetAllEmployees()
        {
            List<Employee> e;
            using(var db = new ApplicationContext())
            {
                e = db.Employees.ToList();
            }
            return e;
        }
    }
}
