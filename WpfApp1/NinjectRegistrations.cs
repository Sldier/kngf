﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<iDataContext>().To<DataContext>();
        }

    }
}
