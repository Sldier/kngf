﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Command;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class CompanyWindowViewModel : BaseModel
    {
        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new RelayCommand(obj =>
                  {
                      var c = new Company();
                      c.Name = CompanyName;
                      using(var db = new ApplicationContext())
                      {
                          db.Companies.Add(c);
                          db.SaveChanges();
                          Companies.Add(c);
                      }
                      SelectedCompany = c;
                  }));
            }
        }

        private RelayCommand editCommand;
        public RelayCommand EditCommand
        {
            get
            {
                return editCommand ??
                    (editCommand = new RelayCommand(obj =>
                    {
                        using (var db = new ApplicationContext())
                        {   
                            var s = db.Companies.Find(SelectedCompany.CompanyId);
                            s.Name = CompanyName;
                            SelectedCompany.Name = CompanyName;
                            db.SaveChanges();
                        }                        
                    },
                    (obj) => SelectedCompany != null));
            }
        }

        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get
            {
                return deleteCommand ??
                    (deleteCommand = new RelayCommand(obj =>
                    {
                        using (var db = new ApplicationContext())
                        {
                            db.Companies.Remove(SelectedCompany);
                            db.SaveChanges();
                            Companies.Remove(SelectedCompany);
                        }
                    },
                    (obj) => SelectedCompany != null));
            }
        }

        private RelayCommand sackCommand;
        public RelayCommand SackCommand => sackCommand ??
            (sackCommand = new RelayCommand(obj =>
            {
                using (var db = new ApplicationContext())
                {
                    var e = db.EmployeeCompany.Where(ec => ec.EmployeeId == SelectedEmployee.CompanyId && ec.CompanyId == SelectedCompany.CompanyId);
                    db.EmployeeCompany.RemoveRange(e);
                    db.SaveChanges();
                    UpdateEmployeeCompanies();
                }
            },
            (obj) => SelectedCompany != null && SelectedEmployee != null));

        private Company selectedCompany;
        public Company SelectedCompany
        {
            get
            {   
                return selectedCompany;
            }
            set
            {
                selectedCompany = value;
                CompanyName = selectedCompany.Name;
                UpdateEmployeeCompanies();
                OnPropertyChanged(nameof(SelectedCompany));
            }
        }

        public void UpdateEmployeeCompanies()
        {
            if (SelectedCompany != null)
            {
                using (var db = new ApplicationContext())
                {
                    var q = $"SELECT e.EmployeeId, e.Name, ec.Position, ec.Salary FROM Employees AS e JOIN EmployeeCompany AS ec ON ec.EmployeeId = e.EmployeeId WHERE ec.CompanyId = {SelectedCompany.CompanyId}";

                    var e = db.RawSqlQuery(q, x => new EmployeeCompanies
                    {
                        CompanyId = int.Parse(x[0].ToString()),
                        Name = x[1].ToString(),
                        Position = x[2].ToString(),
                        Salary = decimal.Parse(x[3].ToString())
                    });

                    Employees = new ObservableCollection<EmployeeCompanies>(e);
                }
            }
        }

        private EmployeeCompanies selectedEmployee;
        public EmployeeCompanies SelectedEmployee
        {
            get
            {
                return selectedEmployee;
            }
            set
            {
                selectedEmployee = value;
                OnPropertyChanged(nameof(SelectedEmployee));
            }
        }

        private string companyName;
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
                OnPropertyChanged(nameof(companyName));
            }
        }

        public ObservableCollection<Company> Companies { get; set; }
        public ObservableCollection<EmployeeCompanies> employees { get; set; }
        public ObservableCollection<EmployeeCompanies> Employees
        {
            get
            {
                return employees;
            }
            set
            {
                employees = value;
                OnPropertyChanged(nameof(Employees));
            }
        }

        public CompanyWindowViewModel()
        {
            using(var db = new ApplicationContext())
            {
                //Написать SQL запрос и выделить цветом компанию с максимальной суммарной зарплатой сотрудников. 
                var m = db.RawSqlQuery(@"SELECT CompanyId, max(Salary) FROM
                    (SELECT ec.CompanyId, sum(ec.Salary) AS Salary FROM EmployeeCompany AS ec
                    GROUP BY 1)",
                   x => new MaxSalaryCompanies()
                   {
                       Id = int.Parse(x[0].ToString()),
                       Salary = decimal.Parse(x[1].ToString())
                   }).First();

                var comp = db.Companies.ToList();
                foreach(var c in comp)
                {
                    if(c.CompanyId == m.Id)
                    {
                        c.Mark = true;                        
                    }
                }
                Companies = new ObservableCollection<Company>(comp);
            }
        }
    }
}
