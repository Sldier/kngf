﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Command;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class SearchCompanyWindowViewModel : BaseModel
    {
        private Company selectedCompany;
        private bool dialogResult;

        public Action CloseAction { get; set; }

        public ObservableCollection<Company> Companies { get; set; }
        public Company SelectedCompany {
            get => selectedCompany;
            set
            {
                selectedCompany = value;
                OnPropertyChanged(nameof(SelectedCompany));
            }
        }

        private RelayCommand selectCompanyCommand;
        public RelayCommand SelectCompanyCommand => selectCompanyCommand ??
            (selectCompanyCommand = new RelayCommand(obj =>
            {
                DialogResult = true;
                CloseAction();
            },
            (obj) => CanSelectCompanyCommand()));

        public bool DialogResult
        {
            get => dialogResult;
            set {
                dialogResult = value;
                OnPropertyChanged(nameof(DialogResult));
            }
        }

        public bool CanSelectCompanyCommand()
        {
            var t = SelectedCompany != null && Salary > 0 && Position != null;                  
            return t;
        }

        private decimal salary;
        public decimal Salary
        { get => salary;
            set { salary = value; OnPropertyChanged(nameof(Salary)); } }

        private string position;
        public string Position
        {
            get => position;
            set { position = value; OnPropertyChanged(nameof(Position)); }
        }

        public SearchCompanyWindowViewModel(Employee employee)
        {
            DialogResult = false;
            using (var db = new ApplicationContext())
            {
                var comp = db.Companies.FromSql(@"SELECT CompanyId, Name FROM Companies AS c
                   WHERE c.CompanyId NOT IN(SELECT ec.CompanyId FROM EmployeeCompany AS ec WHERE ec.EmployeeId = {0})", employee.EmployeeId).ToList();
                Companies = new ObservableCollection<Company>(comp);
            }            
        }

    }
}
