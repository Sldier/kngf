﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Command;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class EmployeeWindowViewModel : BaseModel
    {
        /// <summary>
        /// Реализовать автоматическое выделение цветом, тех сотрудником, зп которых больше 100 т.р. 
        /// </summary>
        const decimal mark_salary = 100000;

        private string employeeName;
        public string EmployeeName
        {
            get
            {
                return employeeName;
            }
            set
            {
                employeeName = value;
                OnPropertyChanged(nameof(EmployeeName));
            }
        }

        private string employeeAddress;
        public string EmployeeAddress
        {
            get
            {
                return employeeAddress;
            }
            set
            {
                employeeAddress = value;
                OnPropertyChanged(nameof(EmployeeAddress));
            }
        }

        private string employeeTelephone;
        public string EmployeeTelephone
        {
            get
            {
                return employeeTelephone;
            }
            set
            {
                employeeTelephone = value;
                OnPropertyChanged(nameof(EmployeeTelephone));
            }
        }

        private Employee selectedEmployee;
        public Employee SelectedEmployee
        {
            get
            {
                return selectedEmployee ?? (selectedEmployee = new Employee());
            }
            set
            {
                selectedEmployee = value;
                EmployeeName = selectedEmployee.Name;
                EmployeeAddress = selectedEmployee.Address;
                EmployeeTelephone = selectedEmployee.Telephone;
                UpdateEmployeeCompanies();
                OnPropertyChanged(nameof(SelectedEmployee));
            }
        }

        public void UpdateEmployeeCompanies()
        {
            if(SelectedEmployee != null)
            {
                using (var db = new ApplicationContext())
                {
                    var q = $"SELECT c.CompanyId, c.Name, ec.Position, ec.Salary FROM Companies AS c JOIN EmployeeCompany AS ec ON c.CompanyId = ec.CompanyId WHERE ec.EmployeeId = {selectedEmployee.EmployeeId}";
                    var e = db.RawSqlQuery(q, x => new EmployeeCompanies
                    {
                        CompanyId = int.Parse(x[0].ToString()),
                        Name = x[1].ToString(),
                        Position = x[2].ToString(),
                        Salary = decimal.Parse(x[3].ToString())
                    });
                    Companies = new ObservableCollection<EmployeeCompanies>(e);
                }
            }
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                  (addCommand = new RelayCommand(obj =>
                  {
                      var e = new Employee();
                      e.Name = EmployeeName;
                      e.Telephone = EmployeeTelephone;
                      e.Address = EmployeeAddress;
                      using (var db = new ApplicationContext())
                      {
                          db.Employees.Add(e);
                          db.SaveChanges();
                          Employees.Add(e);
                      }
                      SelectedEmployee = e;
                  }));
            }
        }

        private RelayCommand editCommand;
        public RelayCommand EditCommand
        {
            get
            {
                return editCommand ??
                    (editCommand = new RelayCommand(obj =>
                    {
                        using (var db = new ApplicationContext())
                        {
                            var s = db.Employees.Find(SelectedEmployee.EmployeeId);
                            s.Name = EmployeeName;
                            SelectedEmployee.Name = EmployeeName;
                            db.SaveChanges();
                        }
                    },
                    (obj) => SelectedEmployee != null && selectedEmployee.EmployeeId != 0));
            }
        }

        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get
            {
                return deleteCommand ??
                    (deleteCommand = new RelayCommand(obj =>
                    {
                        using (var db = new ApplicationContext())
                        {
                            db.Employees.Remove(SelectedEmployee);
                            db.SaveChanges();
                            Employees.Remove(SelectedEmployee);
                        }
                    },
                    (obj) => SelectedEmployee != null && selectedEmployee.EmployeeId != 0));
            }
        }

        private RelayCommand addToCompanyCommand;
        public RelayCommand AddToCompanyCommand => addToCompanyCommand ??
                    (addToCompanyCommand = new RelayCommand(obj =>
                    {
                        var vm = new SearchCompanyWindowViewModel(SelectedEmployee);
                        var w = new SearchCompanyWindow
                        {
                            DataContext = vm
                        };

                        if (vm.CloseAction == null)
                        {
                            vm.CloseAction = new Action(() => w.Close());
                        }
                        
                        w.ShowDialog();
                        if (vm.DialogResult)
                        {
                            using(var db = new ApplicationContext())
                            {
                                var e = db.Employees.Find(SelectedEmployee.EmployeeId);
                                e.EmployeeCompanies.Add(new EmployeeCompany
                                {
                                    CompanyId = vm.SelectedCompany.CompanyId,
                                    EmployeeId = SelectedEmployee.EmployeeId,
                                    Position = vm.Position,
                                    Salary = vm.Salary
                                });
                                UpdateEmployeeCompanies();
                                db.SaveChanges();                                                             
                            }
                        }
                        UpdateSalary();
                        
                    },
                    (obj) => SelectedEmployee != null && selectedEmployee.EmployeeId != 0));

        private RelayCommand sackCommand;
        public RelayCommand SackCommand => sackCommand ??
            (sackCommand = new RelayCommand(obj =>
            {
                using (var db = new ApplicationContext())
                {
                    var e = db.EmployeeCompany.Where(ec => ec.EmployeeId == SelectedEmployee.EmployeeId && ec.CompanyId == SelectedCompany.CompanyId);
                    db.EmployeeCompany.RemoveRange(e);
                    db.SaveChanges();
                    UpdateEmployeeCompanies();
                    UpdateSalary();
                }
            },
            (obj) => SelectedCompany != null && SelectedEmployee != null));

        private ObservableCollection<EmployeeCompanies> companies;
        public ObservableCollection<EmployeeCompanies> Companies {
            get
            {
                return companies;
            }
            set
            {
                companies = value;
                OnPropertyChanged(nameof(Companies));
            }
        }

        private EmployeeCompanies selectedCompany;
        public EmployeeCompanies SelectedCompany
        {
            get
            {
                return selectedCompany;
            }
            set
            {
                selectedCompany = value;
                OnPropertyChanged(nameof(SelectedCompany));
            }
        }

        public ObservableCollection<Employee> Employees { get; set; }

        public void UpdateSalary()
        {
            using (var db = new ApplicationContext())
            {
                foreach (var e in Employees)
                {
                    e.Salary = db.EmployeeCompany.Where(ec => ec.EmployeeId == e.EmployeeId).Sum(p => p.Salary);
                    if (e.Salary > mark_salary)
                    {
                        e.Mark = true;
                    }
                }
            }
                
        }

        public EmployeeWindowViewModel(iDataContext dc)
        {   
            Employees = new ObservableCollection<Employee>(dc.GetAllEmployees());
            UpdateSalary();
        }
    }
}
