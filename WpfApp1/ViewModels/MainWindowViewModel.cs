﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Command;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class MainWindowViewModel
    {
        private IKernel k;

        private RelayCommand openCompanyWindow;
        public RelayCommand OpenCompanyWindow
        {
            get
            {
                return openCompanyWindow ??
                  (openCompanyWindow = new RelayCommand(obj =>
                  {
                      CompanyWindow СompanyWindow = new CompanyWindow() {
                          DataContext = new CompanyWindowViewModel()
                      };

                      СompanyWindow.Show();
                  }));
            }
        }

        private RelayCommand openEmployeeWindow;
        public RelayCommand OpenEmployeeWindow
        {
            get
            {
                return openEmployeeWindow ??
                  (openEmployeeWindow = new RelayCommand(obj =>
                  {
                      EmployeeWindow EmployeeWindow = new EmployeeWindow()
                      {
                          DataContext = new EmployeeWindowViewModel(k.Get<iDataContext>())
                      };

                      EmployeeWindow.Show();
                  }));
            }
        }

        public List<EmployeesWithMaximumSalary> EmployeesWithMaximumSalary { get; set; }

        public MainWindowViewModel(IKernel kernel)
        {
            k = kernel;
            using(var db = new ApplicationContext())
            {
                /// Написать SQL запрос и вывести в отдельную таблицу (компания, ФИО, зп) всех сотрудников с максимальной зарплатой. 
                EmployeesWithMaximumSalary = db.RawSqlQuery(@"SELECT c.Name, e.Name, ec.Salary FROM Companies AS c
                                    JOIN EmployeeCompany AS ec ON ec.CompanyId = c.CompanyId
                                    JOiN Employees AS e ON ec.EmployeeId = e.EmployeeId
                                    WHERE ec.Salary = (SELECT max(ec.Salary) FROM EmployeeCompany AS ec WHERE ec.CompanyId = c.CompanyId)",
                                    x => new EmployeesWithMaximumSalary
                                    {
                                        CompanyName = x[0].ToString(),
                                        EmployeeName = x[1].ToString(),
                                        Salary = x[2].ToString()
                                    });
            }
        }
    }
}
